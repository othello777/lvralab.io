---
weight: 300
title: OpenVR-SpaceCalibrator
---

# OpenVR-SpaceCalibrator

- [OpenVR-SpaceCalibrator (Linux Fork) GitHub Repository](https://github.com/galister/OpenVR-SpaceCalibrator)


OpenVR-SpaceCalibrator (SpaceCal) is a tool for calibrating devices together that are using different tracking universes.

This, for example, allows users to use Vive Trackers or Index Controllers along with ALVR.

Refer to the [Running section](https://github.com/galister/OpenVR-SpaceCalibrator#running) of the readme file for detailed instructions.
